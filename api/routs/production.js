const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Product = require('../models/product');
const checKAuth = require('../middleware/check-auth');
const multer = require('multer');
const upload = multer();
router.get('/',checKAuth,(req,res,next) => {
    Product.find().select('name price _id').exec().then(
        docs =>{
            const response = {
                count:docs.length,
                production:docs.map(doc =>{
                    return{
                    name :doc.name,
                    price :doc.price,
                    _id: doc._id,
                    request:{
                        type:'GET',
                        url:'http://localhost:3000/production/'+doc._id
                    }

                    }
                })
            };
          //  console.log(docs);
            // if(docs.length>=0){
                res.status(200).json(response)
            // }else{
            //     res.status(404).json({
            //         message :"No entries found"
            //     });
            // }
           
        }
    ).catch(err=>{
        console.log(err);
        res.status(500).json({
            error:err
        });
    });
    // res.status(200).json({
    //     message :'Handling get request to/production '
    // });
});


router.post('/',checKAuth,(req,res,next) => {
    
    // const product = {
    //     name: re,q.body.name,
    //     price :req.body.price
    // };
    const product = new Product({
        _id: new mongoose.Types.ObjectId(),
        name:req.body.name,
        price:req.body.price
    });
    product.save().then(result=>{
         console.log(result);
         res.status(201).json({
            message :'Created product succesfully ',
            createdProduct:{
                name :result.name,
                price : result.price,
                _id:result._id,
                request:{
                    type:'GET',
                    URL:"http://localhost:3000/production/"+result._id

                }
            }
        });
    }).catch
    (err => {console.log(err);
     res.status(500).json({
       error: err
     })
     });
    
});

router.get('/:productionid',(req,res,next)  => {
    const pro = req.params.productionid;
    Product.find({pro}).select('name price _id')
    .exec().
    then(doc =>{
       // console.log(doc);
        console.log("from database",doc);
        if(doc){
            res.status(200).json({
                product:doc,
                request:{
                    type:'Get',
                    url:'http//localhost/products'
                }
            });
        }
        else{
            res.status(404).json({
                message :"No valid entry found for provided id"
            });
        }
    }).
    catch(err =>{
        console.log(err);
      res.status(500).json({error:err});
    });
 
    // if(id =='special'){
    //     res.status(200).json({
    //         message:'you discovered the special Id',
    //         id :id
           
    //     });
    // }
        
    //         else{
    //             res.status(200).json({
    //                 massage: 'you passed new id'

    //             });
    //         }
        
    });


    router.patch('/:productionid',checKAuth,(req,res,next)  => {
        const id = req.params.productionid
        const updateOps = {}
        for(const ops of req.body){
            updateOps[ops.propName] = ops.value;
        }
        
        Product.update({_id:id},{$set:updateOps}).exec().then(result => {
            console.log(result);
            res.status(200).json({
                message:'product updated',
                request:{
                    type:'GET',
                    url:'http://localhost:3000/product/'+id
                }
            });

        })
        .catch(err =>{
            console.log(err);
            res.status(500).json({
                error:err
            });
        });
        // res.status(200).json({
        //     massage:'updated product'
        // });

    });
    
    router.delete('/:productionid',checKAuth,(req,res,next)  => {
        const id = req.params.productionid
        Product.remove({_id:id}).exec()
        .then(result =>{
            res.status(200).json({
                message:'Product deleted',
                request:{
                    type:'POST',
                    url:'localhost:3000/production/',
                    body:{
                        name:'String',price:'Number'
                    }
                }
            });
        })
        .catch(err =>{
            console.log(err);
            res.status(500).json({
                error:err
            });
        });
        // res.status(200).json({
        //     massage:'updated delted'
        // });

    });

     //all delete

     router.delete('/',(req,res,next) => {
        Product.remove().exec().then(
            docs =>{
                console.log(docs);
                // if(docs.length>=0){
                    res.status(200).json(docs)
                // }else{
                //     res.status(404).json({
                //         message :"No entries found"
                //     });
                // }
               
            }
        ).catch(err=>{
            console.log(err);
            res.status(500).json({
                error:err
            });
        });
        // res.status(200).json({
        //     message :'Handling get request to/production '
        // });
    });

    module.exports = router;