const http = require('http');
const app = require('./app');
const port = process.env.PORT || 4000;
var server = http.createServer(app);

var io = require('socket.io')(server);

require('./api/routs/chat')(io);

server.listen(port);